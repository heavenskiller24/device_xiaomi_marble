echo 'Starting to clone stuffs needed to build for marble'


# Vendor
echo 'Cloning vendor tree'
rm -rf vendor/xiaomi/marble && git clone https://gitlab.com/heavenskiller24/vendor_xiaomi_marble.git -b aosp-13 vendor/xiaomi/marble


# Kernel
echo 'Cloning kernel tree'
rm -rf device/xiaomi/marble-kernel && git clone https://gitlab.com/heavenskiller24/device_xiaomi_marble-kernel.git -b aosp-13 device/xiaomi/marble-kernel


# Xiaomi
echo 'Cloning hardware xiaomi'
rm -rf hardware/xiaomi && git clone https://gitlab.com/heavenskiller24/hardware_xiaomi.git -b lineage-20 hardware/xiaomi


# Camera
echo 'Cloning Leica Camera'
git clone --depth=1 https://gitlab.com/heavenskiller24/vendor_xiaomi_camera.git -b topaz-leica vendor/xiaomi/camera


echo 'delete vendorsetup.sh from device tree once this is done'
